# SIGs - Special Interest Groups

SIG的全称是Special Interest Groups，即“特别兴趣小组”。MindSpore社区成立各技术SIG的初衷是为该领域的专家、教授和学生提供一个开放交流的平台，通过会议分享、项目开发等活动
促进技术交流、合作共赢，并使得SIG成员的影响力和技术能力得到提升。特别是对于高校的同学们，加入SIG后可以在专家的指导下参与MindSpore的项目开发活动，
掌握业界先进的技术，为将来工作做好准备；也可以和业界专家、教授面对面交流，解决自己在科研中遇到的疑惑，甚至遇到自己的伯乐。截止目前，MindSpore社区
已经成立十多个SIG，不仅包含MindSpore框架本身的前端、后端技术，还包括AI安全、科学计算等上层算法研究，我们也欢迎有想法的的朋友们创建自己的SIG！

## 目前的SIG组

| SIG name                                   | 技术领域                      | SIG 负责人                                                                                                         |
|:-------------------------------------------|:--------------------------|:----------------------------------------------------------------------------------------------------------------|
| [DevelopereXperience](dx/README.md)        | MindSpore-Gitee社区的开发者体验提升 | [@jiancao81](https://gitee.com/jiancao81)(cao-jian@cs.sjtu.edu.cn)                                              |
| [Trusted AI](security/README.md)          | AI安全和隐私保护技术               | [@randywangze](https://gitee.com/randywangze)                                                                   |
| [Usability](usability/README.md)           | MindSpore框架的易用性提升         | [@zhangtong](https://gitee.com/tong-zhang)                                                                      |
| [FrontEnd](frontend/README.md)             | MindSpore前端表达技术           | [@wangnan](https://gitee.com/wangnan39)                                                                         |
| [Compiler](compiler/README.md)             | MindSpore图编译技术            | [@zh_qh](https://gitee.com/zh_qh)                                                                               |
| [Executor](executor/README.md)             | MindSpore后端控制流技术          | [@kisnwang](https://gitee.com/kisnwang)                                                                         |
| [ModelZoo](modelzoo/README.md)             | AI模型的设计和训练                | [@chenhaozhe](https://gitee.com/c_34)                                                                           |
| [Data](data/README.md)                     | 数据处理和增强技术                 | [@liucunwei](https://gitee.com/liucunwei)                                                                       |
| [Visualization](visualization/README.md)   | 模型调试调优可视化技术               | [@liangyongxiong](https://gitee.com/liangyongxiong1024)                                                         |
| [MSLITE](mslite/README.md)                 | 端侧AI技术                    | [@zhaizhiqiang](https://gitee.com/zhaizhiqiang)                                                                 |
| [Parallel](parallel/README.md)             | 自动并行技术                    | [@baiyouhui](https://gitee.com/bert0108)                                                                        |
| [DataCompliance](datacompliance/README.md) | 数据合规风险分析                  | [@gopikrishnanrajbahadur](https://gitee.com/gopikrishnanrajbahadur) [@clement_li](https://gitee.com/clement_li) |
| [MindQuantum](mindquantum/README.md)          | 量子计算软件与算法               | [@dorothy20212021](https://gitee.com/dorothy20212021)                                                                   |

## 学习资源

你可以在MindSpore的哔哩哔哩官方账号观看之前的会议录屏.

 [links](https://space.bilibili.com/526894060/channel/seriesdetail?sid=675044)

## 加入一个SIG

如果你有兴趣加入某个SIG，那么你可以通过以下两种方式加入：

1, 添加MindSpore小助手微信“mindspore0328”，小助手会把你邀请进SIG微信群。

2, 关注MindSpore的微信公众号“MindSpore”，我们会把SIG线上会议的信息用公众号推送，参与线上会议后，你就可以通过SIG负责人分享的二维码加入微信群了。

## 建议一个新的SIG

当你和你的朋友有了非常好的技术idea，并且希望更多的人参与到你们的探索中，那么你就可以申请成立一个新的SIG了，点击下面的链接即可查看申请流程。

[申请流程](https://gitee.com/mindspore/community/blob/master/sigs/dx/docs/How%20to%20build%20a%20SIG%20or%20WG_cn.md)
